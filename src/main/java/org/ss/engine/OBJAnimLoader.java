package org.ss.engine;

import org.lwjgl.opengl.GL;
import org.ss.game.object.Animation;
import java.util.HashMap;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.system.MemoryUtil.NULL;

public class OBJAnimLoader {
    private HashMap<String, Animation> animHashMap;
    private String[] animKeys;

    public OBJAnimLoader(String[] keys, Window window){
        this.animHashMap = new HashMap<String,Animation>();
        this.animKeys = keys;
        this.main(window);
        glfwMakeContextCurrent(NULL);

    }

    public void main(Window window) {
        for (int i = 0; i < animKeys.length; i++) {
            glfwMakeContextCurrent(NULL);
            int finalI = i;
            new Thread(() -> {
                synchronized (this){
                    glfwMakeContextCurrent(window.getWindowHandle());
                    GL.createCapabilities();
                    System.out.print("Loading "+animKeys[finalI]+"\n");
                    try {
                        animHashMap.put(animKeys[finalI],new Animation(animKeys[finalI]));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    glfwMakeContextCurrent(NULL);
                    notify();
                }
            }).start();
            synchronized (this){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public HashMap<String, Animation> getAnimHashMap(){return this.animHashMap;}
}
