package org.ss.engine.graph;

import org.joml.Vector3f;

public class StaticItem {
    private final Mesh[] meshes;

    private final Vector3f position;

    private float scale;

    public StaticItem(Mesh mesh, Vector3f position, float scale){
        this.meshes = new Mesh[]{mesh};
        this.position = position;
        this.scale = scale;
    }
    public StaticItem(Mesh[] meshes, Vector3f position, float scale){
        this.meshes = meshes;
        this.position = position;
        this.scale = scale;
    }

    public StaticItem(Mesh mesh){
        this.meshes = new Mesh[]{mesh};
        this.position = new Vector3f();
        this.scale = 1f;
    }

    public StaticItem(Mesh[] meshes) {
        this.meshes = meshes;
        this.position = new Vector3f();
        this.scale = 1f;
    }

    public StaticItem(){
        this.meshes = null;
        this.position = new Vector3f();
        this.scale = 1f;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Mesh getMesh(){return meshes[0];}

    public Mesh[] getMeshes() {
        return this.meshes;
    }
}
