package org.ss.engine.graph.anim;

import java.util.List;

public class Animation {

    private boolean isFinished;

    private int currentFrame;

    private List<AnimatedFrame> frames;

    private String name;

    private double duration;

    private boolean isAttack;

    private int range;

    private Integer height;

    private int strength;

    private int dangerStart;

    private int dangerEnd;

    private String result;

    public Animation(String name, List<AnimatedFrame> frames, double duration) {
        this.name = name;
        this.frames = frames;
        currentFrame = 0;
        this.duration = duration;
        this.range = -1;
        this.height = -1;
        this.strength = -1;
        this.dangerStart = -1;
        this.dangerEnd = -1;
        this.isAttack = false;
    }

    public AnimatedFrame getCurrentFrame() {
        return this.frames.get(currentFrame);
    }

    public int getFrameIndex(){
        return this.currentFrame;
    }

    public double getDuration() {
        return this.duration;
    }

    public List<AnimatedFrame> getFrames() {
        return frames;
    }

    public String getName() {
        return name;
    }

    public AnimatedFrame getNextFrame() {
        nextFrame();
        return this.frames.get(currentFrame);
    }

    public void nextFrame() {
        int nextFrame = currentFrame + 1;
        if (nextFrame > frames.size() - 1) {
            isFinished = true;
            currentFrame = 0;
        } else {
            isFinished = false;
            currentFrame = nextFrame;
        }
    }
    public void prevFrame() {
        int nextFrame = currentFrame - 1;
        if (nextFrame < 0) {
            isFinished = true;
            currentFrame = frames.size() - 1;
        } else {
            isFinished = false;
            currentFrame = nextFrame;
        }
    }
    public boolean getFinished(){
        return this.isFinished;
    }

    public void setFinished(boolean fin){
        this.isFinished = fin;
    }

    public boolean hasProperties(){
        return range != -1;
    }

    public void setProperties(int range, Integer height, int strength, int dangerStart, int dangerEnd, String result){
        this.range = range;
        this.height =  height;
        this.strength = strength;
        this.dangerStart = dangerStart;
        this.dangerEnd = dangerEnd;
        this.isAttack = true;
        this.result = result;

    }

    public int getRange() {
        return range;
    }

    public Integer getHeight() {
        return height;
    }

    public int getStrength() {
        return strength;
    }

    public int getDangerStart() {
        return dangerStart;
    }

    public int getDangerEnd() {
        return dangerEnd;
    }

    public String getResult() {
        return result;
    }

    public void setFrame(int i) {
        this.currentFrame = 0;
    }
}