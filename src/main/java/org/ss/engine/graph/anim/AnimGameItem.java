package org.ss.engine.graph.anim;

import java.util.Map;
import java.util.Optional;

import org.ss.engine.graph.Mesh;
import org.ss.engine.GameItem;
import org.ss.engine.graph.anim.Animation;

public class AnimGameItem extends GameItem {

    private Map<String, Animation> animations;

    private Animation currentAnimation;

    public String state;

    private boolean reversed;

    public AnimGameItem(Mesh[] meshes, Map<String, Animation> animations) {
        super(meshes);
        this.animations = animations;
        Optional<Map.Entry<String, Animation>> entry = animations.entrySet().stream().findFirst();
        currentAnimation = entry.isPresent() ? entry.get().getValue() : null;
        this.state = "Idle";
        this.reversed = false;

    }

    public Animation getAnimation(String name) {
        return animations.get(name);
    }

    public Animation getCurrentAnimation() {
        return currentAnimation;
    }

    public void getAnimations() {
        System.out.print(animations.keySet());
    }

    public void setCurrentAnimation(Animation currentAnimation) {
        this.currentAnimation = currentAnimation;
    }


}
