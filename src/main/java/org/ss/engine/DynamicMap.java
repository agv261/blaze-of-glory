package org.ss.engine;

import org.ss.engine.graph.Mesh;
import org.ss.game.object.Actor;

import java.util.*;

public class DynamicMap {

    private final Map<Mesh, List<GameItem>> staticMap;

    private Map<Mesh, List<GameItem>> dynamicMap;

    private final List<Actor> actors;

    public DynamicMap(Map<Mesh, List<GameItem>> staticMap, List<Actor> actors){
        this.staticMap = staticMap;
        this.actors = actors;
    }

    public Map<Mesh, List<GameItem>> getGameItems(){
        Map<Mesh, List<GameItem>> map = new HashMap<>();
        for (Actor actor : actors) {
            for (Mesh mesh : actor.getMeshes()) {
                map.put(mesh, Collections.singletonList(actor));
            }
        }
        map.putAll(staticMap);
        return map;
    }

    public void addActor(Actor actor){
        this.actors.add(actor);
    }
}
