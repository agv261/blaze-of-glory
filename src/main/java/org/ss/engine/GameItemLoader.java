package org.ss.engine;

import org.joml.Vector3f;

import org.ss.engine.graph.Mesh;
import org.ss.engine.graph.OBJLoader;
import org.ss.game.object.Player;
import org.ss.game.object.Spearman;

import java.io.File;
import java.util.ArrayList;

public class GameItemLoader implements Runnable{

    public static GameItem[] spawnGameItem(GameItem[] gameItems, Mesh mesh, float scale, float x, float y, float z,
                                           float dX, float dY, float dZ) {
        GameItem gameItem = new GameItem(mesh);
        gameItem.setScale(scale);
        gameItem.setPosition(x, y, z);
        gameItem.setVelocity(dX, dY, dZ);
        return insertGameItem(gameItems, gameItem);
    }

    public static ArrayList<Mesh> loadAnimation(String animationName) throws Exception {
        ArrayList<Mesh> meshList = new ArrayList<Mesh>();
        File dir = new File("src/main/resources/models/"+animationName+"/");
        String[] animDirectory = dir.list();
        if (animDirectory == null){
            System.out.print(dir);
        }
        for (int i = 0; i < animDirectory.length; i++) {
            if (i == 0){
                meshList.add(OBJLoader.loadMesh("/models/"+animationName+"/"+animDirectory[i]));
            }else {
                meshList.add(OBJLoader.loadAnimMesh("/models/" + animationName + "/" + animDirectory[i]));
            }
        }
        return meshList;

    }

    public static GameItem[] spawnNItemsInArea(GameItem[] gameItems, Mesh mesh, int num_items,
                                               float scale, int xAreaDim, int yAreaDim, int zAreaDim) {
        int x, y, z;
        float dX, dY, dZ;
        int startIndexOffset = gameItems.length;
        boolean posConflict;

        for (int i = 0; i < num_items; i++) {
            do {
                posConflict = false;

                x = Utils.randInt(-1 * xAreaDim / 2, xAreaDim / 2);
                y = Utils.randInt(-1 * yAreaDim / 2, yAreaDim / 2);
                z = Utils.randInt(-1 * zAreaDim / 2, zAreaDim / 2);

                dX = Utils.randInt(2,5)*Utils.randFloat(-1.0f, 1.0f);
                dY = Utils.randInt(2,5)*Utils.randFloat(-1.0f, 1.0f);
                dZ = Utils.randInt(2,5)*Utils.randFloat(-1.0f, 1.0f);

                for (GameItem gameItem: gameItems) {
                    Vector3f position = gameItem.getPosition();
                    if ((position.x == x) && (position.y == y) && (position.z == z)) {
                        posConflict = true;
                        break;
                    }
                }
            } while (posConflict);
            gameItems = spawnGameItem(gameItems, mesh, scale, x, y, z, dX, dY, dZ);
        }

        return gameItems;
    }

    public static GameItem[] spawnNItemsAtPosition(GameItem[] gameItems, Mesh mesh, int num_items,
                                                   float scale, Vector3f position) {
        float x, y, z;
        float dX, dY, dZ;
        int startIndexOffset = gameItems.length;
        boolean posConflict;

        for (int i = 0; i < num_items; i++) {
            x = position.x;
            y = position.y;
            z = position.z;

            dX = Utils.randInt(4,8)*Utils.randFloat(-1.0f, 1.0f);
            dY = Utils.randInt(4,8)*Utils.randFloat(-1.0f, 1.0f);
            dZ = Utils.randInt(4,8)*Utils.randFloat(-1.0f, 1.0f);

            gameItems = spawnGameItem(gameItems, mesh, scale, x-2, y-1, z, dX, dY, dZ);
        }

        return gameItems;
    }

    public static GameItem[] insertGameItem(GameItem[] gameItems, GameItem gameItem) {
        GameItem[] newGameItems = new GameItem[gameItems.length + 1];
        System.arraycopy(gameItems, 0, newGameItems, 0, gameItems.length);
        newGameItems[gameItems.length] = gameItem;
        return newGameItems;
    }

    public static GameItem[] spawnGround(GameItem[] gameItems, Mesh groundMesh, byte groundLength, byte scale, byte height) {
        for (byte i=-2; i < groundLength; i++ ){
            GameItem gameItem = new GameItem(groundMesh);
            gameItem.setScale(scale);
            gameItem.setPosition((scale*i), -4.9f+height, -5);
            gameItems = insertGameItem(gameItems, gameItem);
        }
        return gameItems;
    }

    @Override
    public void run() {

    }
}
