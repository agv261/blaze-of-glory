package org.ss.engine;

import java.util.*;

import org.ss.engine.graph.Mesh;
import org.ss.engine.graph.anim.AnimGameItem;
import org.ss.game.object.Actor;
import org.ss.engine.DynamicMap;
import org.ss.game.object.Player;

public class Scene {

    private Map<Mesh, List<GameItem>> meshMap;

    private DynamicMap finalMap;

    private GameItem[] gameItems;

    private ArrayList<Actor> actors;

    private Skybox skybox;

    private SceneLight sceneLight;

    public Scene() {
        meshMap = new HashMap();
    }

    public Map<Mesh, List<GameItem>> getGameMeshes(){return finalMap.getGameItems();}

    public void setGameItems(GameItem[] gameItems) {
        this.gameItems = gameItems;
        int numGameItems = gameItems != null ? gameItems.length : 0;
        for (int i = 0; i < numGameItems; i++) {
            GameItem gameItem = gameItems[i];
                Mesh[] meshes = gameItem.getMeshes();
                for (Mesh mesh : meshes) {
                    List<GameItem> list = meshMap.get(mesh);
                    if (list == null) {
                        list = new ArrayList<>();
                        meshMap.put(mesh, list);
                    }
                    list.add(gameItem);
                }
            }
        }

    public void setActors(Actor[] actors) {
        for (Actor actor : actors){
            this.actors.add(actor);
        }
        finalMap = new DynamicMap(this.meshMap, Arrays.asList(actors));
    }

    public void setActors(Actor actor) {
        ArrayList<Actor> list = new ArrayList<Actor>();
        list.add(actor);
        this.actors = list;
        finalMap = new DynamicMap(this.meshMap, actors);
    }

    public void addActor(Actor actor){
        finalMap.addActor(actor);
    }

    public ArrayList<Actor> getActors() {
        return this.actors;
    }

    public GameItem[] getgameItems(){
        return this.gameItems;
    }

    public Skybox getSkybox() {
        return skybox;
    }

    public void setSkybox(Skybox skyBox) {
        this.skybox = skyBox;
    }

    public SceneLight getSceneLight() {
        return sceneLight;
    }

    public void setSceneLight(SceneLight sceneLight) {
        this.sceneLight = sceneLight;
    }

}
