package org.ss.game;

import org.ss.engine.GameEngine;
import org.ss.engine.IGameLogic;

import java.awt.*;
import java.net.URI;

public class Main {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            int WIDTH = 1800;
            int HEIGHT = 1400;
            IGameLogic gameLogic = new MainGame();
            GameEngine gameEng = new GameEngine("GAME", WIDTH, HEIGHT, vSync, gameLogic);
            Desktop.getDesktop().browse(new URI("https://docs.google.com/forms/d/1J61pwAeG9qatpQU3OOrdi-wVIE5NQkcE0I7eTUt0ii4/edit?usp=sharing"));
            Desktop.getDesktop().browse(new URI("https://drive.google.com/file/d/1CxItMc9Cy3dHgIkmrygA0qQzUQkNEQSU/view?usp=sharing"));
            gameEng.start();
        } catch (Exception excp) {
            excp.getStackTrace();
            System.exit(-1);
        }
    }
}
