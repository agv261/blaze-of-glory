package org.ss.game;

import org.joml.Vector4f;
import org.ss.engine.GameItem;
import org.ss.engine.IHUD;
import org.ss.engine.Window;
import org.ss.engine.graph.*;
import org.ss.game.object.Player;
;import java.awt.*;

public class Hud implements IHUD {

    private static final Font FONT = new Font("Arial", Font.PLAIN, 20);

    private static final String CHARSET = "ISO-8859-1";

    private GameItem[] gameItems;

    private final TextItem statusTextItem;

    private final TextItem bossName;

    private final GameItem playerHealth;

    private final GameItem enemyHealth;

    private final Window window;

    private int offset;

    public Hud(String statusText, Window window) throws Exception {
        this.window = window;
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);
        this.statusTextItem = new TextItem(statusText, fontTexture);
        this.statusTextItem.getMesh().getMaterial().setAmbientColour(new Vector4f(0, 1, 0, 1));
        this.statusTextItem.setScale(1.85f);

        // Create health bars
        Mesh mesh = OBJLoader.loadMesh("/models/bar.obj");
        Material material = new Material();
        material.setAmbientColour(new Vector4f(0, 1, 0, 1));
        mesh.setMaterial(material);
        playerHealth = new GameItem(mesh);
        playerHealth.setScale(40.0f);
        this.playerHealth.setPosition(750f, 100f, 0);

        this.bossName = new TextItem("The Old Guard", fontTexture);
        this.bossName.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 0, 0, 1));
        this.bossName.setScale(5f);

        Mesh bossMesh = OBJLoader.loadMesh("/models/bar.obj");
        Material mat = new Material();
        mat.setAmbientColour(new Vector4f(1, 0, 0, 1));
        bossMesh.setMaterial(mat);
        enemyHealth = new GameItem(bossMesh);
        enemyHealth.setScale(80.0f);
        enemyHealth.setPosition(window.getWidth(), window.getHeight()-100, 0);

        // Create list that holds the items that compose the HUD
        gameItems = new GameItem[]{statusTextItem, playerHealth};
    }

    public void setStatusText(String statusText) {
        this.statusTextItem.setText(statusText);
    }

    @Override
    public GameItem[] getGameItems() {
        return gameItems;
    }

    public void updateSize(Window window) {
        this.statusTextItem.setPosition(0,  100f, 1);
        this.bossName.setPosition(window.getWidth()-650, window.getHeight()-200,0);
        this.enemyHealth.setPosition(window.getWidth()+offset, window.getHeight()-100,0);
    }

    public void updateEnemyHealth(int healthPerc){
        offset = 1400-(healthPerc*14);
        this.enemyHealth.setPosition(window.getWidth()+offset, window.getHeight()-100, 0);

    }

    public void updateHealth(int healthPerc) throws Exception {
        this.playerHealth.setPosition(750f / 100f * healthPerc, 100f, 0);

    }

    public void displayBoss(){
        gameItems = new GameItem[]{statusTextItem, playerHealth, enemyHealth, bossName};
    }

    public void displayGameOver() throws Exception {
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);
        TextItem gameOver = new TextItem("GAME OVER", fontTexture);
        gameOver.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 0, 0, 0));
        gameOver.setScale(10f);
        gameOver.setPosition(window.getWidth()/2, window.getHeight()/2,0);
        gameItems = new GameItem[]{gameOver};
    }

    public void displayWin() throws Exception {
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);
        TextItem gameOver = new TextItem("CONGRATULATIONS!", fontTexture);
        gameOver.getMesh().getMaterial().setAmbientColour(new Vector4f(0, 1, 0, 0));
        gameOver.setScale(10f);
        gameOver.setPosition(window.getWidth()/2 -500, window.getHeight()/2,0);
        gameItems = new GameItem[]{gameOver};
    }
}
