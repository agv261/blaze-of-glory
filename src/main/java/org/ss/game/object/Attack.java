package org.ss.game.object;

import org.ss.engine.GameItem;

public class Attack extends Animation{

    private float range;

    private byte height;

    private final byte strength;

    private int dangerStart;

    private int dangerEnd;

    public String result;

    private GameItem target;

    public Attack(String path2Attack, float range, byte height, int dangerStart, int dangerEnd, String result) throws Exception {
        super(path2Attack, false,false);
        this.range = range;
        this.height = height;
        this.dangerStart = dangerStart;
        this.dangerEnd = dangerEnd;
        this.strength = 1;
        this.result = result;
    }

    public boolean canHit(){
        return (this.getFrame() > dangerStart && this.getFrame() < dangerEnd);
    }

    public String detectCollision(float distance){
        if (distance <= this.range) return this.result;

        else  return "MISS";
    }

    public void setResult(String hit) {
        this.result = hit;
    }

    public void setDangerEnd(int i) {
        this.dangerEnd = i;
    }

    public void setRange(float v) {
        this.range = v;
    }

    public void setHeight(byte b) {
        this.height = b;
    }

    public void setDangerStart(int i) {
        this.dangerStart = i;
    }
}