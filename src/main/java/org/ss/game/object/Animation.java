package org.ss.game.object;

import org.ss.engine.GameItemLoader;
import org.ss.engine.graph.Mesh;

import java.util.ArrayList;

public class Animation{

    private boolean reversible; //Animation can be played backwards instead of using a Rev

    private boolean inverting; //Actor faces other opposite direction @ the end

    private final ArrayList<Mesh> meshes;

    private final ArrayList<Mesh> revMeshes;

    private int counter;

    private final int animationSize;

    private boolean finished = false;

    private int range;


    public Animation(String path2Animation, boolean inverting, boolean reversible) throws Exception {
        if (reversible){
            this.reversible = true;
            this.revMeshes = null;
        } else {
            this.reversible = false;
            this.revMeshes = GameItemLoader.loadAnimation(path2Animation + "-Rev");
        }
        this.inverting = inverting;
        this.meshes = GameItemLoader.loadAnimation(path2Animation);
        counter = 0;
        animationSize = meshes.size();
    }

    public Animation(String path2Animation) throws Exception {
        this.meshes = GameItemLoader.loadAnimation(path2Animation);
        this.revMeshes = GameItemLoader.loadAnimation(path2Animation + "-Rev");
        this.inverting = false;
        this.reversible = false;
        this.counter = 0;
        this.animationSize = this.meshes.size();
    }

    public Animation(String path2Animation, boolean oneTime) throws Exception {

        this.meshes = GameItemLoader.loadAnimation(path2Animation);
        this.revMeshes = null;
        this.inverting = false;
        this.reversible = false;
        this.counter = 0;
        this.animationSize = this.meshes.size();

    }

    public Mesh play(boolean reverse){
        if (reverse){
           return playReverse();
        } else {
            this.finished = false;
            Mesh newMesh = this.meshes.get(counter);
            if (counter == animationSize - 1) {
                this.finished = true;
                if (!this.reversible) {
                    counter = 0;
                }
            } else {
                counter++;
            }
            return newMesh;
        }
    }

    public Mesh playFlipped(boolean reverse){
        if (reverse){
            return playReverse2();
        } else {
            this.finished = false;
            if (this.reversible) {
                return playReverse();
            } else {
                Mesh newMesh = this.revMeshes.get(counter);
                if (counter == animationSize - 1) {
                    this.finished = true;
                    counter = 0;
                } else {
                    counter++;
                }
                return newMesh;
            }
        }
    }

    public Mesh playReverse(){
        if (this.finished && counter == 0){
            counter = animationSize - 1;
            this.finished = false;
        }

        Mesh newMesh = this.meshes.get(counter);
        if (counter == 0){
            this.finished=true;
        } else {
            this.finished=false;
            counter--;

        }
        return newMesh;
    }

    public Mesh playReverse2(){
        if (this.finished && counter == 0){
            counter = animationSize - 1;
            this.finished = false;
        }

        Mesh newMesh = this.revMeshes.get(counter);
        if (counter == 0){
            this.finished=true;
        } else {
            this.finished=false;
            counter--;

        }
        return newMesh;
    }

    public Mesh interrupt(boolean reversed){
        System.out.print("Interrupting current animation");
        this.setFinished(false);
        if (this.reversible){
            this.counter = this.getSize()-1;
        } else {
            this.counter = 0;
        }
        if (reversed){
            return this.meshes.get(0);
        } else {
            return this.revMeshes.get(0);
        }
    }

    public int getFrame(){
        return this.counter;
    }

    public int getSize(){
        return this.animationSize;
    }

    public boolean getInverting(){
        return this.inverting;
    }

    public void setInverting(boolean inv){this.inverting = inv;}

    public void setReversible(boolean rev){this.reversible = rev;}

    public boolean getFinished() { return this.finished; }

    public void setFinished(boolean fin){ this.finished = fin; }
}
