package org.ss.game.object;
import org.ss.engine.assimp.AnimMeshesLoader;
import org.ss.engine.graph.anim.AnimGameItem;
import org.ss.engine.graph.anim.Animation;

import java.util.Locale;

public class Player extends Actor {

    private AnimGameItem dodges;

    private AnimGameItem attacks;

    private AnimGameItem blocks;

    private AnimGameItem hits;

    public boolean hasHit;

    private int elevation;

    private boolean combo;

    private int comboCounter=1;


    public Player() throws Exception {
        this.hasHit = false;
        this.state = "Idle";
        this.setRotation(0, 90, 0);

        this.elevation = 1;

        dodges = AnimMeshesLoader.loadAnimGameItem(System.getProperty("user.dir")+"\\models\\player\\Player-Dodges.fbx", System.getProperty("user.dir")+"textures\\");
        dodges.setPosition(this.getPosition());

        attacks = AnimMeshesLoader.loadAnimGameItem(System.getProperty("user.dir")+"\\models\\player\\Player-Attacks.fbx", System.getProperty("user.dir")+"textures\\");
        attacks.setPosition(this.getPosition());
        attacks.getAnimation("metarig|attack1").setProperties(8,1,3,15,17,"Hit");
        attacks.getAnimation("metarig|attack2").setProperties(8,1,4,7,9,"Hit");
        attacks.getAnimation("metarig|attack3").setProperties(8,1,5,17,19,"Pushed");
        attacks.getAnimation("metarig|counter").setProperties(10,1,5,13,15,"Pushed");

        hits = AnimMeshesLoader.loadAnimGameItem(System.getProperty("user.dir")+"\\models\\player\\Player-Hits.fbx", System.getProperty("user.dir")+"textures\\");
        hits.setPosition(this.getPosition());
        this.actions = new AnimGameItem[]{dodges, attacks, hits};
        this.meshes = actions[0].getMeshes();
        this.curr = actions[0];

    }

    public void backflip() {
        elevation = 3;
        this.meshes = dodges.getMeshes();
        this.curr = dodges;
        dodges.setCurrentAnimation(dodges.getAnimation("_metarig|backflip"));
        Animation animation = dodges.getCurrentAnimation();
        int frame = animation.getFrameIndex();
        if (animation.getFinished()) {
            this.state = "Idle";
            animation.setFinished(false);
            this.elevation = 1;
        } else {
            if (frame > 5 && frame < 65) {

                if (!this.reversed) {
                    this.setVelocity(-15, 0, 0);
                } else {
                    this.setVelocity(15, 0, 0);
                }
            } else if (frame > 65 && frame <= 86) {

                if (!this.reversed) {
                    this.setVelocity(this.getVelocity().x + .75f, 0, 0);
                } else {
                    this.setVelocity(this.getVelocity().x - .75f, 0, 0);
                }
            }
            animation.nextFrame();
        }
    }

    public void cartwheel() {
        elevation = 2;
        this.meshes = dodges.getMeshes();
        this.curr = dodges;
        dodges.setCurrentAnimation(dodges.getAnimation("_metarig|cartwheel"));
        Animation animation = dodges.getCurrentAnimation();
        int frame = animation.getFrameIndex();
        if (animation.getFinished()) {
            reverse(animation);
            this.elevation = 1;
        } else {
            if (frame > 12 && frame < 48) {

                this.circleAround();
            } else if (frame >= 51 && frame < 70) {
                if (!this.reversed) {
                    this.setVelocity(6, this.getVelocity().y, this.getVelocity().z);
                } else {
                    this.setVelocity(-6, this.getVelocity().y, this.getVelocity().z);
                }
            }
            animation.nextFrame();
        }
    }

    public void circleAround() {
        float x, y, z;
        this.radians = (float) (this.radians + Math.PI / 36);
        x = (float) (18 * Math.cos(this.radians - Math.PI / 2));
        y = this.getVelocity().y;
        z = (float) (15 * Math.sin(this.radians - Math.PI / 2));
        this.setVelocity(x, y, z);
    }

    public void slide() {
        elevation = 0;
        this.meshes = dodges.getMeshes();
        this.curr = dodges;
        dodges.setCurrentAnimation(dodges.getAnimation("_metarig|slide"));
        Animation animation = dodges.getCurrentAnimation();
        int frame = animation.getFrameIndex();
        if (animation.getFinished()) {
            reverse(animation);
            this.elevation = 1;
        } else {
            if (frame <= 48) {
                this.ovalAround();
            } else if (frame < 57) {
                if (!this.reversed) {
                    this.setVelocity(6, this.getVelocity().y, this.getVelocity().z);
                } else {
                    this.setVelocity(-6, this.getVelocity().y, this.getVelocity().z);
                }
            }
            animation.nextFrame();
        }
    }

    public void ovalAround() {
        float x, y, z;
        this.radians1 = (float) (this.radians1 + Math.PI / 48);
        x = (float) (15 * Math.cos(this.radians1 - Math.PI / 2));
        y = this.getPosition().y;
        z = (float) (9 * Math.sin(this.radians1 - Math.PI / 2));
        this.setVelocity(x, y, z);

    }

    private void reverse(Animation animation) {
        this.state = "Idle";
        animation.setFinished(false);
        if (!reversed) {
            System.out.print(radians);
            this.setRotation(0, -90, 0);
            this.radians = this.radians1 = (float) Math.PI;
            this.reversed = true;

        } else {
            this.setRotation(0, 90, 0);
            this.radians1 = this.radians = 0;
            this.reversed = false;
        }
        this.setPosition(this.getPosition().x, this.getPosition().y, -5);
    }

    public void attack(Spearman enemy) throws Exception {
        this.meshes = attacks.getMeshes();
        this.curr = attacks;
        attacks.setCurrentAnimation(attacks.getAnimation("metarig|attack"+comboCounter));
        Animation animation = attacks.getCurrentAnimation();
        if (animation.getFrameIndex() == 24 && comboCounter == 1 && combo){
            reset();
            combo = false;
            comboCounter += 1;
            hasHit = false;
            attacks.setCurrentAnimation(attacks.getAnimation("metarig|attack"+comboCounter));
            animation.nextFrame();
        }
        if (animation.getFinished()) {
            if (combo && comboCounter < 3){
                reset();
                combo = false;
                comboCounter += 1;
                hasHit = false;
                attacks.setCurrentAnimation(attacks.getAnimation("metarig|attack"+comboCounter));
                animation.nextFrame();
            } else {
            this.state = "Idle";
            this.comboCounter = 1;
            this.combo = false;
            this.attacks.setCurrentAnimation(attacks.getAnimation("metarig|attack1"));
            }
            animation.setFinished(false);
        } else if (!hasHit){
            detectCollision(enemy);
        }
        animation.nextFrame();
    }

    public boolean handleCollision(String result, Integer height, int strength) throws Exception {
        if (!this.state.equals("Blocking") && (this.elevation == 1 || height == this.elevation)) {
            System.out.print("hit! by" + result+ "\n");
            return getHit(result, strength);
        } else if (this.state.equals("Blocking") && result == "Sweeped") {
            return getHit(result, strength);
        } else if (this.state == "Blocking") {
            System.out.print("attack blocked!"+"\n");
            block();
            return true;
        }
        return false;
    }

    private boolean getHit(String result, int strength) throws Exception {
        this.state = result;
        this.meshes = hits.getMeshes();
        this.curr = hits;
        Animation anim = hits.getAnimation("metarig|" + result.toLowerCase(Locale.ROOT));
        this.damage(strength);
        this.updateHealth();
        hits.setCurrentAnimation(anim);
        getUp();
        return true;
    }

    public void getUp() {
        this.elevation = -1;
        Animation animation = hits.getCurrentAnimation();
        int frame = animation.getFrameIndex();
        animation.nextFrame();
        if (animation.getFinished()) {
            this.state = "Idle";
            animation.setFinished(false);
            this.elevation = 1;
            this.setPosition(this.getPosition().x, this.getPosition().y, -5);
        } else if (this.state.equals("Knocked") || this.state.equals("Sweeped")) {
            if (frame < 21) {
                if (reversed) {
                    this.setVelocity((25 - frame), 0, 0);
                } else {
                    this.setVelocity(-(25 - frame), 0, 0);
                }
            }
        }
    }

    public void block() {
        this.curr = attacks;
        this.meshes = attacks.getMeshes();
        attacks.setCurrentAnimation(attacks.getAnimation("metarig|block1"));
        Animation animation = attacks.getCurrentAnimation();
        if (animation.getFinished()) {
            this.state = "Idle";
            animation.setFinished(false);
        } else {
            animation.nextFrame();
            if (reversed && animation.getFrameIndex() < 20) {
                this.setVelocity(8, 0, 0);
            } else if (!reversed && animation.getFrameIndex() < 20) {
                this.setVelocity(-8, 0, 0);
            }
        }
    }

    public void setRev(boolean reversed) {
        this.reversed = reversed;
    }

    public void setElevation(int i) {
        this.elevation = i;
    }

    public void reset() {
        if (!reversed) {
            System.out.print(radians);
            this.setRotation(0, 90, 0);
            this.radians = this.radians1 = 0;

        } else {
            this.setRotation(0, -90, 0);
            this.radians = this.radians1 = (float) Math.PI;
        }

        for (AnimGameItem item: actions){
            item.getCurrentAnimation().setFrame(0);

        }
    }

    public void counter(Spearman enemy) throws Exception {
        this.meshes = attacks.getMeshes();
        this.curr = attacks;
        attacks.setCurrentAnimation(attacks.getAnimation("metarig|counter"));
        Animation animation = attacks.getCurrentAnimation();
        if (animation.getFinished()) {
            this.state = "Idle";
            animation.setFinished(false);
            attacks.setCurrentAnimation(attacks.getAnimation("metarig|attack1"));
        } else if (!hasHit) {
            detectCollision(enemy);
        }
        animation.nextFrame();
    }

    private void detectCollision(Spearman enemy) throws Exception {
        Animation anim = attacks.getCurrentAnimation();
        int frame = anim.getFrameIndex();
        float distance = Math.abs(this.getPosition().x - enemy.getPosition().x);
        if (anim.getDangerStart() <= frame && anim.getDangerEnd() <= frame && anim.getRange() > distance) {
            this.hasHit = enemy.handleCollision(anim.getResult(), anim.getStrength(), distance);
        }
    }

    public void setCombo(){
        this.combo = true;
    }
}

