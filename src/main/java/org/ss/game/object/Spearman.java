package org.ss.game.object;

import org.ss.engine.assimp.AnimMeshesLoader;
import org.ss.engine.graph.Material;
import org.ss.engine.graph.Mesh;

import java.util.Locale;
import java.util.Random;

import org.ss.engine.graph.anim.AnimGameItem;
import org.ss.engine.graph.anim.Animation;
import org.ss.game.Hud;

public class Spearman extends Actor{

    private AnimGameItem stances;

    private AnimGameItem attacks;

    public boolean hasHit;

    public Spearman() throws Exception {

        stances = AnimMeshesLoader.loadAnimGameItem(System.getProperty("user.dir")+"\\models\\Pike\\Pike-Stance.fbx",System.getProperty("user.dir")+"textures\\");

        attacks = AnimMeshesLoader.loadAnimGameItem(System.getProperty("user.dir")+"\\models\\Pike\\Pike-Attacks.fbx",System.getProperty("user.dir")+"textures\\");

        this.actions = new AnimGameItem[]{stances, attacks};
        this.meshes = stances.getMeshes();
        stances.setCurrentAnimation(stances.getAnimation("metarig|battle"));
        this.curr = stances;
        this.hasHit = false;


    }

    public void act(Player player) throws Exception {
        this.setVelocity(0,0,0);
        float distance = this.getPosition().x - player.getPosition().x;

        switch (this.state) {
            case "Idle":
                if (distance < 15) {
                    this.meshes = stances.getMeshes();
                    this.curr = stances;
                    stances.setCurrentAnimation(stances.getAnimation("metarig|battle"));
                    Animation animation = stances.getCurrentAnimation();
                    animation.nextFrame();
                    if (stances.getAnimation("metarig|battle").getFinished()) {
                        this.state = "Battle";
                    }
                }
                break;
            case "Turning":
                turnAround();
                break;
            case "Battle":
                move(distance, player);
                break;
            case "Walking":
                walk();
                break;

            case "Attacking":
                attack(player,distance);
                break;

            case "Backing":
                backup(distance);
                break;

            case "Hit":

            case "Pushed":
                recover();
                break;

            case "Blocking":
                block();
                break;


        }
    }

    private void move(float distance, Player player) throws Exception {
        System.out.print(distance);
        if (isBehind(distance)){
            stances.setCurrentAnimation(stances.getAnimation("metarig|turn"));
            this.state= "Turning";
        } else if (Math.abs(distance) > 8) {
            this.state="Walking";
            stances.setCurrentAnimation(stances.getAnimation("metarig|walk"));
            walk();
        } else if (Math.abs(distance) < 3){
            stances.setCurrentAnimation(stances.getAnimation("metarig|walk"));
            this.state="Backing";
            backup(distance);
        } else {
            attack(player,distance);
        }
    }

    private boolean isBehind(float distance) {
        return (!this.reversed && distance < 0) || (this.reversed && distance > 0);
    }

    private Animation selectAttack(){
        Random rand = new Random();

        switch (rand.nextInt(4)){
            case 0:
                Animation animation = this.attacks.getAnimation("metarig|stab1");
                animation.setProperties(20,1, 5, 25, 28,
                        "Knocked");
                return animation;
            case 1:
                Animation anim = this.attacks.getAnimation("metarig|sweep");
                anim.setProperties(12,0, 10, 48, 51,
                        "Sweeped");
                return anim;
            case 2:
                anim = this.attacks.getAnimation("metarig|swath");
                anim.setProperties(12,2, 10, 69, 71,
                        "Sweeped");
                return anim;
            case 3:
                anim = this.attacks.getAnimation("metarig|slam");
                anim.setProperties(15,3, 25, 57, 60,
                        "Slammed");
                return anim;
        }


        return null;
    }

    public void attack(Player player, float distance) throws Exception {
        this.meshes = attacks.getMeshes();
        this.curr = attacks;
        if (this.state.equals("Battle")) {
            this.state = "Attacking";
            attacks.setCurrentAnimation(selectAttack());
            hasHit = false;
        }
        Animation anim = attacks.getCurrentAnimation();
        int frame = anim.getFrameIndex();
        if (anim.getDangerStart() <= frame && anim.getDangerEnd() <= frame){
            if (anim.getRange() > distance && !hasHit) {
                this.hasHit = player.handleCollision(anim.getResult(), anim.getHeight(), anim.getStrength());
            }
        }

        this.attacks.getCurrentAnimation().nextFrame();
        if (this.attacks.getCurrentAnimation().getFinished()) {
            this.state = "Battle";
        }
    }

    public boolean handleCollision(String result, int strength, float distance) throws Exception {
        if (isBehind(distance) || this.state.equals("Turning") || this.state.equals("Walking")){
            this.state = result;
            stances.setCurrentAnimation(stances.getAnimation("metarig|" + result.toLowerCase(Locale.ROOT)));
            System.out.print("Ennemy got"+result+"!\n ");
            this.damage(strength);
            updateHealth();
            recover();
            return true;
        } else if (!this.state.equals("Attacking")){
            System.out.print(reversed+"\n");
            System.out.print(distance+"\n");
            this.state = "Blocking";
            this.damage(Math.floorDiv(strength,2));
            updateHealth();
            block();
            return true;
        }
        return false;
    }

    private void block() {
        this.meshes = stances.getMeshes();
        this.curr = stances;
        stances.setCurrentAnimation(stances.getAnimation("metarig|block"));
        Animation animation = stances.getCurrentAnimation();
        animation.nextFrame();
        if (animation.getFinished()) {
            System.out.print("animation finished");
            this.state = "Battle";
            animation.setFinished(false);
        }
    }

    private void recover() {
        this.meshes = stances.getMeshes();
        this.curr = stances;
        Animation animation = stances.getCurrentAnimation();
        animation.nextFrame();
        if (this.state == "Hit"){
            if (animation.getFinished()) {
                this.state = "Battle";
                animation.setFinished(false);
            }
        } else if (this.state == "Pushed"){
            if (animation.getFinished()) {
                this.state = "Battle";
                animation.setFinished(false);
            }

        }


    }

    private void turnAround(){
        Animation animation = stances.getAnimation("metarig|turn");
        stances.setCurrentAnimation(animation);
        if (animation.getFinished()){
            this.state="Battle";
            this.reversed = !this.reversed;
            animation.setFinished(false);
            this.setRotation(0,this.getRotation().y-180,0);
        } else {
            animation.nextFrame();
        }

    }

    private void walk(){
        this.meshes = stances.getMeshes();
        this.curr = stances;
        Animation animation = stances.getCurrentAnimation();
        animation.nextFrame();
        if (animation.getFinished()){
            this.state = "Battle";
        }
        if (reversed && animation.getFrameIndex() < 20) {
            this.setVelocity(6, 0, 0);
        } else if (!reversed && animation.getFrameIndex() < 20) {
            this.setVelocity(-6, 0, 0);
        }
    }

    private void backup(float distance){
        this.meshes = stances.getMeshes();
        this.curr = stances;
        Animation animation = stances.getCurrentAnimation();
        animation.prevFrame();
        if (animation.getFinished()){
            this.state = "Battle";
        }
        if (reversed) {
            this.setVelocity(-4, 0, 0);
        } else {
            this.setVelocity(4, 0, 0);
        }
    }

    private void animateAttack(Attack attack, boolean reverse) {
        Material mat;
        mat = this.getMesh().getMaterial();
        Mesh newMesh;
        if (this.reversed) {
            newMesh = attack.playFlipped(reverse);
        } else {
            newMesh = attack.play(reverse);
        }
        newMesh.setMaterial(mat);
        this.setMesh(newMesh);

        if (attack.getFinished()) {
            if (attack.getInverting() && this.reversed) {
                this.reversed = false;
            } else if (attack.getInverting() && !this.reversed) {
                this.reversed = true;
            }
            this.setPosition(this.getPosition().x, this.getPosition().y, -5);
        }
    }
    public void updateHealth() throws Exception {
        System.out.print("this is the guard's health: " + this.getHealthPerc()+"\n");
        hud.updateEnemyHealth(this.getHealthPerc());
    }
}
