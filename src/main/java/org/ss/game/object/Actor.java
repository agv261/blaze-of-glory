package org.ss.game.object;

import org.ss.engine.GameItem;
import org.ss.engine.graph.Mesh;
import org.ss.engine.graph.anim.AnimGameItem;
import org.ss.engine.graph.anim.Animation;
import org.ss.game.Hud;

public class Actor extends GameItem {

    public AnimGameItem curr;

    public Animation currentAnimation;

    public String state;

    private int healthPerc;

    public float radians;

    public float radians1;

    public boolean reversed;

    public Hud hud;

    public Mesh[] meshes;

    public AnimGameItem[] actions;

    public Actor() throws Exception{

        this.setPosition(0,0,-5);
        this.setScale(.06f);
        this.setVelocity(0, 0, 0);
        this.state = "Idle";
        this.healthPerc = 100;
        this.reversed = false;
        this.meshes = null;
        this.curr = null;
        this.actions = null;
    }

    @Override
    public Mesh[] getMeshes() {
        return this.meshes;
    }

    public AnimGameItem getCurr(){
        return this.curr;
    }

    public boolean getReversed(){
        return this.reversed;
    }

    public AnimGameItem[] getActions(){
        return this.actions;
    }

    public void setHud(Hud hud){
        this.hud = hud;
    }

    public void updateHealth() throws Exception {
        System.out.print("this is the player's health: " + healthPerc+"\n");
        hud.updateHealth(healthPerc);
    }

    public int getHealthPerc() {
        return healthPerc;
    }

    public void damage(int dmg){
        this.healthPerc -= dmg;
    }
}
