package org.ss.game;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

import org.ss.engine.*;
import org.ss.engine.graph.*;
import org.ss.engine.graph.anim.AnimGameItem;
import org.ss.engine.graph.anim.AnimatedFrame;
import org.ss.game.object.Actor;

import java.util.List;
import java.util.Map;

public class Renderer{

    private static final float FOV = (float) Math.toRadians(60.0f);

    private static final float Z_NEAR = 0.01f;

    private static final float Z_FAR = 1000.0f;

    private static final int MAX_POINT_LIGHTS = 5;

    private static final int MAX_SPOT_LIGHTS = 5;

    private final Transformation transformation;

    private ShaderProgram shaderProgram;

    private ShaderProgram skyboxShaderProgram;

    private ShaderProgram depthShaderProgram;

    private ShaderProgram hudShaderProgram;

    private final float specularPower;

    public Renderer() {
        transformation = new Transformation();
        specularPower = 10f;
    }

    public void init(Window window) throws Exception {
        // Create shader
        setupSkyboxShader();
        setupSceneShader();
        setupDepthShader();
        setupHudShader();
    }
    private void setupSkyboxShader() throws Exception {
        skyboxShaderProgram = new ShaderProgram();
        skyboxShaderProgram.createVertexShader(Utils.loadResource("/shaders/skybox_vertex.vs"));
        skyboxShaderProgram.createFragmentShader(Utils.loadResource("/shaders/skybox_fragment.fs"));
        skyboxShaderProgram.link();

        // Create uniforms for projection matrix
        skyboxShaderProgram.createUniform("projectionMatrix");
        skyboxShaderProgram.createUniform("modelViewMatrix");
        skyboxShaderProgram.createUniform("texture_sampler");
        skyboxShaderProgram.createUniform("ambientLight");
    }

    private void setupSceneShader() throws Exception {
        shaderProgram = new ShaderProgram();
        shaderProgram.createVertexShader(Utils.loadResource("/shaders/anim_vertex.vs"));
        shaderProgram.createFragmentShader(Utils.loadResource("/shaders/anim_fragment.fs"));
        shaderProgram.link();

        // Create uniforms for modelView and projection matrices
        shaderProgram.createUniform("projectionMatrix");
        shaderProgram.createUniform("modelViewMatrix");
        shaderProgram.createUniform("texture_sampler");

        // Create uniforms for material
        shaderProgram.createMaterialUniform("material");

        // Create uniform for default color and color flag that controls it
        shaderProgram.createUniform("specularPower");
        shaderProgram.createUniform("ambientLight");
        shaderProgram.createPointLightListUniform("pointLights", MAX_POINT_LIGHTS);
        shaderProgram.createSpotLightListUniform("spotLights", MAX_SPOT_LIGHTS);
        shaderProgram.createDirectionalLightUniform("directionalLight");

        shaderProgram.createUniform("jointsMatrix");
    }

    private void setupDepthShader() throws Exception {
        depthShaderProgram = new ShaderProgram();
        depthShaderProgram.createVertexShader(Utils.loadResource("/shaders/depth_vertex.vs"));
        depthShaderProgram.createFragmentShader(Utils.loadResource("/shaders/depth_fragment.fs"));
        depthShaderProgram.link();

        depthShaderProgram.createUniform("orthoProjectionMatrix");
        depthShaderProgram.createUniform("modelLightViewMatrix");

        // Create uniform for joint matrices
        depthShaderProgram.createUniform("jointsMatrix");
    }

    private void setupHudShader() throws Exception {
        hudShaderProgram = new ShaderProgram();
        hudShaderProgram.createVertexShader(Utils.loadResource("/shaders/hud_vertex.vs"));
        hudShaderProgram.createFragmentShader(Utils.loadResource("/shaders/hud_fragment.fs"));
        hudShaderProgram.link();

        // Create uniforms for Ortographic-model projection matrix and base colour
        hudShaderProgram.createUniform("projModelMatrix");
        hudShaderProgram.createUniform("color");
        hudShaderProgram.createUniform("hasTexture");
    }

    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public void render(Window window, Camera camera, Scene scene, IHUD hud) {
        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }

        renderSkyBox(window, camera, scene);

        shaderProgram.bind();

        // Update the projection matrix
        Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        shaderProgram.setUniform("projectionMatrix", projectionMatrix);

        // Update view Matrix
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);

        // Update Light Uniforms
        renderLights(viewMatrix, scene.getSceneLight());

        renderSkyBox(window,camera,scene);

        renderScene(window, camera, scene);

        renderHud(window, hud);

    }
    private void renderSkyBox(Window window, Camera camera, Scene scene) {
        skyboxShaderProgram.bind();

        skyboxShaderProgram.setUniform("texture_sampler", 0);

        Matrix4f projectionMatrix = transformation.getProjectionMatrix();
        skyboxShaderProgram.setUniform("projectionMatrix", projectionMatrix);
        Skybox skybox = scene.getSkybox();
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);
        viewMatrix.m30(0);
        viewMatrix.m31(0);
        viewMatrix.m32(0);
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(skybox, viewMatrix);
        skyboxShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
        skyboxShaderProgram.setUniform("ambientLight", scene.getSceneLight().getAmbientLight());

        scene.getSkybox().getMesh().render();

        skyboxShaderProgram.unbind();
    }

    private void renderScene(Window window, Camera camera, Scene scene) {
        shaderProgram.bind();

        // Update the projection matrix
        Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        shaderProgram.setUniform("projectionMatrix", projectionMatrix);

        // Update view Matrix
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);

        // Update Light Uniforms
        renderLights(viewMatrix, scene.getSceneLight());

        shaderProgram.setUniform("texture_sampler", 0);
        // Render each gameItem
        renderGameItems(scene, viewMatrix);
        //renderActors(scene,viewMatrix);
        shaderProgram.unbind();
    }

//    private void renderActors(Scene scene, Matrix4f viewMatrix) {
//        Mesh[] player = scene.getPlayer().get(0).getMeshes();
//        for (Mesh mesh : player) {
//            shaderProgram.setUniform("material", mesh.getMaterial());
//            mesh.renderList( scene.getPlayer(), (Actor actor) -> {
//
//                        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(actor, viewMatrix);
//                        shaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
//                        AnimGameItem animGameItem = actor.getCurr();
//                        AnimatedFrame frame = animGameItem.getCurrentAnimation().getCurrentFrame();
//                        shaderProgram.setUniform("jointsMatrix", frame.getJointMatrices());
//                    }
//            );
//        }
//    }

    private void renderGameItems(Scene scene, Matrix4f viewMatrix) {
//        GameItem[] gameItems = scene.getgameItems();
//        for (GameItem gameItem : gameItems) {
//            if (gameItem != null){
//                Mesh mesh = gameItem.getMesh();
//
//                // Set modelView matrix for this item
//                Matrix4f modelViewMatrix = transformation.getModelViewMatrix(gameItem, viewMatrix);
//                shaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
//
//                // Render the mesh
//                shaderProgram.setUniform("material", mesh.getMaterial());
//                mesh.render();
//            }
//        }
        Map<Mesh, List<GameItem>> mapMeshes = scene.getGameMeshes();
        for (Mesh mesh : (mapMeshes.keySet())) {

            shaderProgram.setUniform("material", mesh.getMaterial());
            mesh.renderList2(mapMeshes.get(mesh), (GameItem gameItem) -> {

                        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(gameItem, viewMatrix);
                        shaderProgram.setUniform("modelViewMatrix", modelViewMatrix);

                        if ( gameItem instanceof Actor ) {
                            AnimGameItem animGameItem = ((Actor)gameItem).getCurr();
                            AnimatedFrame frame = animGameItem.getCurrentAnimation().getCurrentFrame();
                            shaderProgram.setUniform("jointsMatrix", frame.getJointMatrices());
                        }
                    }
            );

        }

    }

//    private void renderDepthMap(Window window, Camera camera, Scene scene) {
//        // Setup view port to match the texture size
//        glBindFramebuffer(GL_FRAMEBUFFER, shadowMap.getDepthMapFBO());
//        glViewport(0, 0, ShadowMap.SHADOW_MAP_WIDTH, ShadowMap.SHADOW_MAP_HEIGHT);
//        glClear(GL_DEPTH_BUFFER_BIT);
//
//        depthShaderProgram.bind();
//
//        DirectionalLight light = scene.getSceneLight().getDirectionalLight();
//        Vector3f lightDirection = light.getDirection();
//
//        float lightAngleX = (float)Math.toDegrees(Math.acos(lightDirection.z));
//        float lightAngleY = (float)Math.toDegrees(Math.asin(lightDirection.x));
//        float lightAngleZ = 0;
//        Matrix4f lightViewMatrix = transformation.updateLightViewMatrix(new Vector3f(lightDirection).mul(light.getShadowPosMult()), new Vector3f(lightAngleX, lightAngleY, lightAngleZ));
//        DirectionalLight.OrthoCoords orthCoords = light.getOrthoCoords();
//        Matrix4f orthoProjMatrix = transformation.updateOrthoProjectionMatrix(orthCoords.left, orthCoords.right, orthCoords.bottom, orthCoords.top, orthCoords.near, orthCoords.far);
//
//        depthShaderProgram.setUniform("orthoProjectionMatrix", orthoProjMatrix);
//        Map<Mesh, List<GameItem>> mapMeshes = scene.getGameMeshes();
//        for (Mesh mesh : mapMeshes.keySet()) {
//            mesh.renderList(mapMeshes.get(mesh), (GameItem gameItem) -> {
//                        Matrix4f modelLightViewMatrix = transformation.buildModelViewMatrix(gameItem, lightViewMatrix);
//                        depthShaderProgram.setUniform("modelLightViewMatrix", modelLightViewMatrix);
//
//                        if ( gameItem instanceof AnimGameItem ) {
//                            AnimGameItem animGameItem = (AnimGameItem)gameItem;
//                            AnimatedFrame frame = animGameItem.getCurrentFrame();
//                            depthShaderProgram.setUniform("jointsMatrix", frame.getJointMatrices());
//                        }
//                    }
//            );
//        }
//
//        // Unbind
//        depthShaderProgram.unbind();
//        glBindFramebuffer(GL_FRAMEBUFFER, 0);
//    }

    private void renderLights(Matrix4f viewMatrix, SceneLight sceneLight) {

        shaderProgram.setUniform("ambientLight", sceneLight.getAmbientLight());
        shaderProgram.setUniform("specularPower", specularPower);

        // Process Point Lights
        PointLight[] pointLightList = sceneLight.getPointLightList();
        int numLights = pointLightList != null ? pointLightList.length : 0;
        for (int i = 0; i < numLights; i++) {
            // Get a copy of the point light object and transform its position to view coordinates
            PointLight currPointLight = new PointLight(pointLightList[i]);
            Vector3f lightPos = currPointLight.getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;
            shaderProgram.setUniform("pointLights", currPointLight, i);
        }

        // Process Spot Ligths
        SpotLight[] spotLightList = sceneLight.getSpotLightList();
        numLights = spotLightList != null ? spotLightList.length : 0;
        for (int i = 0; i < numLights; i++) {
            // Get a copy of the spot light object and transform its position and cone direction to view coordinates
            SpotLight currSpotLight = new SpotLight(spotLightList[i]);
            Vector4f dir = new Vector4f(currSpotLight.getConeDirection(), 0);
            dir.mul(viewMatrix);
            currSpotLight.setConeDirection(new Vector3f(dir.x, dir.y, dir.z));

            Vector3f lightPos = currSpotLight.getPointLight().getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;

            shaderProgram.setUniform("spotLights", currSpotLight, i);
        }

        // Get a copy of the directional light object and transform its position to view coordinates
        DirectionalLight currDirLight = new DirectionalLight(sceneLight.getDirectionalLight());
        Vector4f dir = new Vector4f(currDirLight.getDirection(), 0);
        dir.mul(viewMatrix);
        currDirLight.setDirection(new Vector3f(dir.x, dir.y, dir.z));
        shaderProgram.setUniform("directionalLight", currDirLight);

    }

    private void renderHud(Window window, IHUD hud) {
        hudShaderProgram.bind();

        Matrix4f ortho = transformation.getOrthoProjectionMatrix(0, window.getWidth(), window.getHeight(), 0);
        for (GameItem gameItem : hud.getGameItems()) {
            Mesh mesh = gameItem.getMesh();
            // Set ortohtaphic and model matrix for this HUD item
            Matrix4f projModelMatrix = transformation.getOrtoProjModelMatrix(gameItem, ortho);
            hudShaderProgram.setUniform("projModelMatrix", projModelMatrix);
            hudShaderProgram.setUniform("color", gameItem.getMesh().getMaterial().getAmbientColour());
            hudShaderProgram.setUniform("hasTexture", gameItem.getMesh().getMaterial().isTextured() ? 1 : 0);

            // Render the mesh for this HUD item
            mesh.render();
        }

        hudShaderProgram.unbind();
    }

    public void cleanup() {
        if (shaderProgram != null) {
            shaderProgram.cleanup();
        }
    }
}
