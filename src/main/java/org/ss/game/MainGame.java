package org.ss.game;

import org.joml.Vector3f;
import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector4f;
import org.ss.engine.*;
import org.ss.engine.graph.*;
import org.ss.game.object.Actor;
import org.ss.game.object.Player;
import org.ss.game.object.Spearman;

public class MainGame implements IGameLogic {

    private static final float MOUSE_SENSITIVITY = 0.2f;

    private final Vector3f cameraVelocity;

    private final Vector3f playerVelocity;

    private final Renderer renderer;

    private final Camera camera;

    private GameItem[] gameItems;

    private Spearman Pike;

    private Scene scene;

    private Hud hud;

    private Player human;

    private float lightAngle;

    private static final float CAMERA_POS_STEP = 0.05f;

    private String state;

    public MainGame() {
        renderer = new Renderer();
        camera = new Camera();
        camera.setPosition(2f, 6f, 15f);
        cameraVelocity = new Vector3f(0.0f, 0.0f, 0.0f);
        playerVelocity = new Vector3f(0.0f, 0.0f, 0.0f);
        lightAngle = -90;
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        state = "GAME";

        scene = new Scene();


        float reflectance = 1f;
        Mesh cubeMesh = OBJLoader.loadMesh("/models/rampart.obj");
        Texture cubeTexture = new Texture(System.getProperty("user.dir")+"\\textures\\ramparts.png");
        Material cubeMaterial = new Material(cubeTexture, reflectance);
        cubeMesh.setMaterial(cubeMaterial);

        gameItems = new GameItem[]{};

        Pike = new Spearman();
        Pike.setPosition(40,0,-5);

        gameItems = GameItemLoader.spawnGround(gameItems, cubeMesh, (byte) 10, (byte) 25, (byte) -8);
        scene.setGameItems(gameItems);

        human = new Player();
        human.setVelocity(playerVelocity);
        scene.setActors(human);
        scene.addActor(Pike);

        Skybox skybox = new Skybox(OBJLoader.loadMesh("/models/cube.obj"), System.getProperty("user.dir")+"\\textures\\castle-background.png");
        scene.setSkybox(skybox);

        // Setup Lights
        setupLights();

        hud = new Hud("DEMO",window);

        human.setHud(hud);
        Pike.setHud(hud);
    }
    private void setupLights() {
        SceneLight sceneLight = new SceneLight();
        scene.setSceneLight(sceneLight);

        // Ambient Light
        sceneLight.setAmbientLight(new Vector3f(1.0f, 1.0f, 1.0f));

        // Directional Light
        float lightIntensity = 0.25f;
        Vector3f lightPosition = new Vector3f(-1, 0, 0);
        sceneLight.setDirectionalLight(new DirectionalLight(new Vector3f(1, 1, 1), lightPosition, lightIntensity));
    }

    @Override
    public boolean input(Window window, MouseInput mouseInput) throws Exception {
        hud.updateSize(window);
        hud.setStatusText("HP: ");


        if (human.getHealthPerc() <= 0){
            hud.displayGameOver();
            state = "MENU";
        } else if (Pike.getHealthPerc() <= 0){
            hud.displayWin();
            state = "MENU";
        }

        switch (state){
            case "GAME":
                cameraVelocity.set(playerVelocity.x,playerVelocity.y,playerVelocity.z/2);
                playerVelocity.set(0,0,0);
                // Change camera position keys
                if (window.isKeyPressed(GLFW_KEY_A) && human.state.equals("Idle") && human.getPosition().x > 1) {
                    playerVelocity.x -= 5;


                } else if (window.isKeyPressed(GLFW_KEY_D) && human.state.equals("Idle") && human.getPosition().x > - 45) {
                    playerVelocity.x += 5;

                    if (human.getPosition().x >= 15){
                        hud.displayBoss();
                    }
                }

                if (human.state.equals("Backflipping")){
                    human.backflip();
                } else if (window.isKeyPressed(GLFW_KEY_X)  && human.state.equals("Idle")){
                    human.state= "Backflipping";
                    human.reset();
                    human.backflip();
                }

                if (human.state.equals("Cartwheeling")){
                    human.cartwheel();
                } else if (window.isKeyPressed(GLFW_KEY_C) && human.state.equals("Idle")){
                    human.state = "Cartwheeling";
                    human.reset();
                    human.cartwheel();

                }

                if (human.state.equals("Sliding")){
                    human.slide();
                } else if (window.isKeyPressed(GLFW_KEY_S) && human.state.equals("Idle")){
                    human.state = "Sliding";
                    human.reset();
                    human.slide();
                }

                if (human.state.equals("Slammed") || human.state.equals("Knocked") || human.state.equals("Hit")
                || human.state.equals("Sweeped")){
                    human.getUp();
                }

                // Attack
                if (human.state.equals("Attacking")){
                    if (mouseInput.isLeftButtonPressed() && human.curr.getCurrentAnimation().getFrameIndex() > 10){
                        human.setCombo();
                    }
                    human.attack(Pike);
                } else if (mouseInput.isLeftButtonPressed()  && human.state.equals("Idle")){
                    human.hasHit = false;
                    human.state= "Attacking";
                    human.reset();
                    human.attack(Pike);
                }

                if (human.state.equals("Countering")){
                    human.counter(Pike);
                }

                // "Block attacks"
                if (human.state.equals("Blocking")){
                    if (mouseInput.isLeftButtonPressed()){
                        human.state = "Countering";
                        human.reset();
                        human.counter(Pike);
                    } else {
                        human.block();
                    }

                }else if (mouseInput.isRightButtonPressed() && human.state.equals("Idle")){
                    human.state = "Blocking";
                    human.reset();
                    human.block();
                }
                Pike.act(human);
                return true;

            case "MENU":
                TextItem txt = (TextItem) hud.getGameItems()[0];
                Vector4f color = txt.getMesh().getMaterial().getAmbientColour();
                if (color.w < 1) {
                    txt .getMesh().getMaterial().setAmbientColour(new Vector4f(color.x, color.y, color.z, color.w + .01f));
            }
                break;
        }
        return false;
    }

    @Override
    public void update(float interval, MouseInput mouseInput) {
        camera.movePosition(cameraVelocity.x * CAMERA_POS_STEP, cameraVelocity.y * CAMERA_POS_STEP, cameraVelocity.z * CAMERA_POS_STEP);

        if (human.getReversed()){
            human.movePosition(-playerVelocity.z * CAMERA_POS_STEP, playerVelocity.y * CAMERA_POS_STEP, playerVelocity.x * CAMERA_POS_STEP);
        } else {
            human.movePosition(playerVelocity.z * CAMERA_POS_STEP, playerVelocity.y * CAMERA_POS_STEP, -playerVelocity.x * CAMERA_POS_STEP);
        }

        if (Pike.getReversed()) {
            Pike.movePosition(-Pike.getVelocity().x * CAMERA_POS_STEP, Pike.getVelocity().y * CAMERA_POS_STEP, Pike.getVelocity().z * CAMERA_POS_STEP);
        } else {
            Pike.movePosition(Pike.getVelocity().x * CAMERA_POS_STEP, Pike.getVelocity().y * CAMERA_POS_STEP, -Pike.getVelocity().z * CAMERA_POS_STEP);
        }
        SceneLight sceneLight = scene.getSceneLight();

        // Update directional light direction, intensity and colour
        DirectionalLight directionalLight = sceneLight.getDirectionalLight();
        lightAngle += 0.1f;
        if (lightAngle > 90) {
            directionalLight.setIntensity(0);
            if (lightAngle >= 360) {
                lightAngle = -90;
            }
            sceneLight.getAmbientLight().set(0.3f, 0.3f, 0.4f);
        } else if (lightAngle <= -80 || lightAngle >= 80) {
            float factor = 1 - (Math.abs(lightAngle) - 80) / 10.0f;
            sceneLight.getAmbientLight().set(factor, factor, factor);
            directionalLight.setIntensity(factor);
            directionalLight.getColor().y = Math.max(factor, 0.9f);
            directionalLight.getColor().z = Math.max(factor, 0.5f);
        } else {
            sceneLight.getAmbientLight().set(1, 1, 1);
            directionalLight.setIntensity(1);
            directionalLight.getColor().x = 1;
            directionalLight.getColor().y = 1;
            directionalLight.getColor().z = 1;
        }
        double angRad = Math.toRadians(lightAngle);
        directionalLight.getDirection().x = (float) Math.sin(angRad);
        directionalLight.getDirection().y = (float) Math.cos(angRad);
    }

    @Override
    public void render(Window window) {
        renderer.render(window, camera, scene, hud);
    }

    @Override
    public void cleanup() {
        renderer.cleanup();
        for (GameItem gameItem : gameItems) {
            if (gameItem instanceof Actor){
                for (Mesh mesh : gameItem.getMeshes()){
                    mesh.cleanUp();
                }
            } else {
                for (Mesh mesh : gameItem.getMeshes()) {
                    mesh.cleanUp();
                }
            }
        }
    }
}
